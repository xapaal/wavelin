package com.XaviPastor.sinte;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.TextView;
import android.widget.Toast;

public class graph {
	
	int[] partidas;
	int[] colores;
	int numero=0;
	static Context cnt;
	static int altura;
	static int anchura;
	static Canvas mCanvas;
	static Paint paint;
	static int distancia_x;
	static int x0, x1, y0, y1;
	static int buffer;
	
	static Bitmap resultado = null;
	
	static double distancia_y;
	
	
	public int getnumero(){
		return numero;
	}
	
	public int getscore(int la){
		return partidas[la];
	}
	
	public boolean cargar(Context c, int al, int an){
		
		
		cnt = c;
		anchura=an; altura=al;
		resultado = Bitmap.createBitmap(anchura, altura, Bitmap.Config.ARGB_8888);  
		distancia_y = ((float)altura/(float)(200));
		mCanvas = new Canvas(resultado); 
		paint = new Paint();
		
			return false;
	}
	
	public void actbufferescala(int coso){
		buffer = coso;
	}
	
	
	
	
	public static Bitmap getBMP(int[] samples){
		//pre:  altura/anchura!=0, short[] con length buffer (int de entrada)
		//post: introduces height y witdh del imageview, valores en forma de short de la eq y el lenght del short[]
		//numero es buffer
		
//		samples[0]=0;

//				toast("max: "+getMax(samples));
		
		for(int i=1; i < buffer; i++){
			if (samples[i]>100||samples[i]<-100)
				samples[i]=samples[i-1];
	   }
		
		distancia_x = anchura/(buffer);
		
		paint.setColor(Color.WHITE);
		
		if(buffer<2000){
		paint.setStrokeWidth((cnt.getResources().getDimensionPixelSize(R.dimen.uno)));
		}
		if (buffer<200){
			paint.setStrokeWidth((cnt.getResources().getDimensionPixelSize(R.dimen.dos)));
		}
		if (buffer<35){
			paint.setStrokeWidth((cnt.getResources().getDimensionPixelSize(R.dimen.tres)));
		}
		
		mCanvas.drawColor(Color.parseColor("#000000"));
		paint.setColor(Color.parseColor("#AAAAAA"));
		
		x0 = 0;
		y0 = (int)(altura/2+samples[0]*distancia_y);
		x1 = 1*distancia_x;
		y1 = (int)(altura/2+samples[1]*distancia_y);
		
		

		mCanvas.drawLine(x0, y0, x1, y1,
				paint);

		for (int a=1; a<buffer-1; a++){

			x0 = a*distancia_x;
			y0 = (int)(altura/2+samples[a]*distancia_y);
			x1 = (a+1)*distancia_x;
			y1 = (int)(altura/2+samples[a+1]*distancia_y);
			
			mCanvas.drawLine(x0, y0, x1, y1, paint);
		}
		
		return resultado;
	}
	
	

	public static void toast(String a) {
		Toast toast = Toast.makeText(cnt, a,
				Toast.LENGTH_LONG);
		toast.show();
	}

	public void setEscala(){
		
	}
	
	public static int getMax(int[] v){
		int res=v[0];
		
		for (int i=1; i<v.length; i++){
			if (v[i]>res)
				res=v[i];
		}
			
		return res;
	}
	
}
