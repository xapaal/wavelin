package com.XaviPastor.sinte;

import java.io.BufferedReader;

import com.google.analytics.tracking.android.EasyTracker;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ClickableViewAccessibility")
public class instrument extends Activity {
	
	boolean decay=false;
	
	int c=0, cs=0, d=0, ds=0, e=0, f=0, fs=0, g=0, gs=0, a=0, as=0, b=0;
	
//	TextView texto;
	private AudioManager audio;
	
	TextView textView;
	
	int sonando;
	
	RelativeLayout[] pes = new RelativeLayout[28];
	
    float[] x = new float[10];
    float[] y = new float[10];
    boolean[] touched = new boolean[10];
    int[] id = new int[10];
    
    float height;
	
	float max1, max2, max3, max4;//maximos
	double mv1, mv2, mv3, mv4;//contadores
	boolean bajar1;
	
	RelativeLayout one, twoo, three, four;
	int y_cord2, resultado;
	boolean moving=false, moving2=false;
	double twopi = 8.*Math.atan(1.);
	boolean isRunning = true;
	
	int seleccionada;
	
	float contador=0;
	
	
	float[] notas = {
		48,99f,//sol
		51,91f,//sol#
		55f,//la
		58,27f,//la#
		61,73f,//si
		
		//2
		65,40f,//do
		69,29f,//do#
		73,41f,//re
		77,78f,//re#
		82,40f,//mi
		87,3f,//	fa
		92,49f,//fa#
		97.99f,//sol
		103.84f,//sol#
		110,//la
		116.54f,//la#
		123.47f,//si
		
		//3
		130.81f,//do
		138.59f,//do#
		146.83f,//re
		155.56f,//re#
		164.81f,//mi
		174.61f,//fa
		184.99f,//fa#
		195.99f,//sol
		207.65f,//sol#
		220,//la
		233.08f,//la#
		246.94f,//si
		
		//4
		261.62f,//do
		277.18f,//do#
		293.66f,//re
		311.12f,//re#
		329.62f,//mi
		349.22f,//fa
		369.99f,//fa#
		391.99f,//sol
		415.30f,//sol#
		440.00f,//la
		466.16f,//la#
		493.88f,//si
		
		//5
		523.25f,//do
		554.36f,//do#
		587.33f,//re
		622.25f,//re#
		659.25f,//mi
		698.45f,//fa
		739.98f,//fa#
		783.99f,//sol
		830.60f,//sol#
		880f,//la
		932,32f,//la#
		987,76f,//si
		1046,5f,//do
		1108,7f,//do#
		1174,6f,//re
		1244,5f,//re#
		1318,5f,//mi
		1396,9f,//fa
		1479,9f,//fa#
		
		};
	
	boolean playing = true;
	
	int margen=4000;
	
	int i =0;
	
	
	float E5=659.25f;
	float A4=440;
	float D4=293.66f;
	float G3=195.99f;
	
	
	float E4=329.63f;
	float A3=220;
	float D3=146.83f;
	float G2=97.999f;
	
	float E3=164.81f;
	float A2=110;
	float D2=73.41f;
	float G1=48.999f;
	
	
	
	public AudioTrack audioTrack;
	Thread t;
    int sr = 44100;
    double sliderval;
    int buffsizeaudio2=0;
    int buffsizeaudio = 8;
    int buffsizeimg = 8;
    short samples[] = new short[buffsizeaudio];
    int ampx =5000;
    double fr = 0;
    double fr2 = 440;
    double ph = 0.0;
    float scaleWidth, scaleHeight;
    float volume=0;
    int escala=16;
    int mod_osc1=0, mod_osc2=0, mod_osc3=0;//0>SINE, 1>TRI, 2>SQU
    double osc1, osc2, osc3;
    double osc11, osc22, osc33;
    double osc1_old=0, osc2_old=0, osc3_old=0;
    int[]samples2;
    boolean iniciado=false;
    
    float cont;
    float des1, des2, des3;
    float releas1=1, releas2=1, releas3=1;
    float maxtr1, maxtr2, maxtr3;
    double nuevo1, nuevo2, nuevo3;
    boolean tr_sub1, tr_sub2, tr_sub3;
    float tremolo1, tremolo2, tremolo3;
	int potint1, potint2, potint3, potint4, potint5, potint6, potint7, potint8, potint9, potint10, potint11, potint12, potint13, potint14, potint15, potint16, potint17, potint18, potint100;
	float volume1=1, volume2=0, volume3=0;
	float freq1=0.1f, freq2=0.1f, freq3=0.1f;
	float trem1=0, trem2=0, trem3=0;
   
    public int duration = 3; // seconds
	public int sampleRate = 8000;
    public int numSamples = duration * sampleRate;
    public double sample[] = new double[numSamples];
    public double freqOfTone = 440; // hz
    Handler handler = new Handler();
    public byte generatedSnd[] = new byte[2 * numSamples];
    
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.instrument);
        
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        
        try{
        	
        	Bundle extras = getIntent().getExtras(); 
        	volume1 = extras.getFloat("volume1");
        	volume2 = extras.getFloat("volume2");
        	volume3 = extras.getFloat("volume3");
        	des1 = extras.getFloat("des1");
        	des2 = extras.getFloat("des2");
        	des3 = extras.getFloat("des3");
        	releas1 = extras.getFloat("releas1");
        	releas2 = extras.getFloat("releas2");
        	releas3 = extras.getFloat("releas3");
        	maxtr1 = extras.getFloat("maxtr1");
        	maxtr2 = extras.getFloat("maxtr2");
        	maxtr3 = extras.getFloat("maxtr3");
        	mod_osc1 = extras.getInt("mod_osc1");
        	mod_osc2 = extras.getInt("mod_osc2");
        	mod_osc3 = extras.getInt("mod_osc3");
        	freq1 = extras.getFloat("freq1");
        	freq2 = extras.getFloat("freq2");
        	freq3 = extras.getFloat("freq3");
        	trem1 = extras.getFloat("trem1");
        	trem2 = extras.getFloat("trem2");
        	trem3 = extras.getFloat("trem3");
        	tremolo1 = extras.getFloat("tremolo1");
        	tremolo2 = extras.getFloat("tremolo2");
        	tremolo3 = extras.getFloat("tremolo3");
        	tr_sub1 = extras.getBoolean("tr_sub1");
        	tr_sub2 = extras.getBoolean("tr_sub2");
        	tr_sub3 = extras.getBoolean("tr_sub3");
        	
        	
        	
        	
        }
        catch (Exception E){}
        
        if (volume1==0&&volume2==0&&volume3==0){
        	volume1=1;
        }
        
        one = (RelativeLayout) findViewById(R.id.one);
        twoo = (RelativeLayout) findViewById(R.id.twoo);
        three = (RelativeLayout) findViewById(R.id.three);
        four = (RelativeLayout) findViewById(R.id.four);
        
        pes[0] = (RelativeLayout) findViewById(R.id.a1);
        pes[1] = (RelativeLayout) findViewById(R.id.a2);
        pes[2] = (RelativeLayout) findViewById(R.id.a3);
        pes[3] = (RelativeLayout) findViewById(R.id.a4);
        pes[4] = (RelativeLayout) findViewById(R.id.a5);
        pes[5] = (RelativeLayout) findViewById(R.id.a6);
        pes[6] = (RelativeLayout) findViewById(R.id.a7);
        pes[7] = (RelativeLayout) findViewById(R.id.a8);
        pes[8] = (RelativeLayout) findViewById(R.id.a9);
        pes[9] = (RelativeLayout) findViewById(R.id.a10);
        pes[10] = (RelativeLayout) findViewById(R.id.a11);
        pes[11] = (RelativeLayout) findViewById(R.id.a12);
        pes[12] = (RelativeLayout) findViewById(R.id.a13);
        pes[13] = (RelativeLayout) findViewById(R.id.a14);
        pes[14] = (RelativeLayout) findViewById(R.id.a15);
        pes[15] = (RelativeLayout) findViewById(R.id.a16);
        pes[16] = (RelativeLayout) findViewById(R.id.a17);
        pes[17] = (RelativeLayout) findViewById(R.id.a18);
        pes[18] = (RelativeLayout) findViewById(R.id.a19);
        pes[19] = (RelativeLayout) findViewById(R.id.a20);
        pes[20] = (RelativeLayout) findViewById(R.id.a21);
        pes[21] = (RelativeLayout) findViewById(R.id.a22);
        pes[22] = (RelativeLayout) findViewById(R.id.a23);
        pes[23] = (RelativeLayout) findViewById(R.id.a24);
        pes[24] = (RelativeLayout) findViewById(R.id.a25);
        pes[25] = (RelativeLayout) findViewById(R.id.a26);
        pes[26] = (RelativeLayout) findViewById(R.id.a27);
        pes[27] = (RelativeLayout) findViewById(R.id.a28);
        
        cargar();
        
        setColor(pes[0], g);
        setColor(pes[1], gs);
        setColor(pes[2], a);
        setColor(pes[3], as);
        setColor(pes[4], b);
        setColor(pes[5], c);
        setColor(pes[6], cs);
        
        setColor(pes[7], d);
        setColor(pes[8], ds);
        setColor(pes[9], e);
        setColor(pes[10], f);
        setColor(pes[11], fs);
        setColor(pes[12], g);
        setColor(pes[13], gs);
        
        setColor(pes[14], a);
        setColor(pes[15], as);
        setColor(pes[16], b);
        setColor(pes[17], c);
        setColor(pes[18], cs);
        setColor(pes[19], d);
        setColor(pes[20], ds);
        
        setColor(pes[21], e);
        setColor(pes[22], f);
        setColor(pes[23], fs);
        setColor(pes[24], g);
        setColor(pes[25], gs);
        setColor(pes[26], a);
        setColor(pes[27], as);
        
        
        
        
        t = new Thread() {
	         public void run() {
	        	 
	        	 if (playing==true){
	        setPriority(Thread.MAX_PRIORITY);
	         generate();
	         
	        	 }
	    }
	   };
	   t.start();
	   
        
        
        one.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            	if (sonando==0||sonando==1){
            		sonando=1;
            	decay=false;
            	moving=true;
            	int pointerCount = event.getPointerCount();
            	for (int i = 0; i < 10; i++) {
                    if (i >= pointerCount) continue;
                    
				if (MotionEvent.ACTION_DOWN == event.getAction()) {
					
                	 y[i] = (int) event.getY(i);
                	 y_cord2 = (int) event.getX(i);
                	 contador=0;
                	 
            	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
            		decay=false;
            		y[i] = (int) event.getY(i);  
            		int y_cord = (int) event.getX(i);
                    x[i] = Math.abs(1-(y_cord-y_cord2));
            	}
            	else if (MotionEvent.ACTION_UP == event.getAction()) {
            		sonando=0;
            		moving=false;
            		one.setBackgroundColor(Color.parseColor("#000000"));
            		decay=true;
            		mv2=mv1=0;}
            	else if (262 == event.getAction()) {
            		moving=true;
                   	pointerCount--;
    				}}
            	
            	float yy = y[0];
            	mv1=x[0];
            	for (int i=1;i<pointerCount; i++){
            		if (yy<y[i]){yy=y[i];mv1=x[i];}}
				if(moving==true){
				
				fr =  E5 + (E5*yy/margen)*(1776.0f/height);
				ajustarfreq(one, 1);
//				llamar();
				}}
            	return true;}});
        
        
        
        twoo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            	if (sonando==0||sonando==2){
            		sonando=2;
            	decay=false;
            	moving=true;
            	int pointerCount = event.getPointerCount();
            	for (int i = 0; i < 10; i++) {
                    if (i >= pointerCount) continue;
                    
				if (MotionEvent.ACTION_DOWN == event.getAction()) {
					
                	 y[i] = (int) event.getY(i);
                	 y_cord2 = (int) event.getX(i);
                	 contador=0;
                	 
            	
            	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
            		decay=false;
            		y[i] = (int) event.getY(i);  
            		int y_cord = (int) event.getX(i);
                    x[i] = Math.abs(1-(y_cord-y_cord2));
            	}
            	else if (MotionEvent.ACTION_UP == event.getAction()) {
            		sonando=0;
            		moving=false;
            		twoo.setBackgroundColor(Color.parseColor("#000000"));
            		decay=true;
            		mv2=mv1=0;}
            	else if (262 == event.getAction()) {
            		moving=true;
                   	pointerCount--;
    				}}
            	
            	float yy = y[0];
            	mv1=x[0];
            	for (int i=1;i<pointerCount; i++){
            		if (yy<y[i]){yy=y[i];mv1=x[i];}}
				if(moving==true){
				
				fr =  A4 + (A4*yy/margen)*(1776.0f/height);
				ajustarfreq(twoo, 2);
//				llamar();
				}}
            	return true;}});
        
        
        three.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            	if (sonando==0||sonando==3){
            		sonando=3;
            	decay=false;
            	moving=true;
            	int pointerCount = event.getPointerCount();
            	for (int i = 0; i < 10; i++) {
                    if (i >= pointerCount) continue;
                    
				if (MotionEvent.ACTION_DOWN == event.getAction()) {
					
                	 y[i] = (int) event.getY(i);
                	 y_cord2 = (int) event.getX(i);
                	 contador=0;
                	 
            	
            	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
            		decay=false;
            		y[i] = (int) event.getY(i);  
            		int y_cord = (int) event.getX(i);
                    x[i] = Math.abs(1-(y_cord-y_cord2));
            	}
            	else if (MotionEvent.ACTION_UP == event.getAction()) {
            		sonando=0;
            		moving=false;
            		three.setBackgroundColor(Color.parseColor("#000000"));
            		decay=true;
            		mv2=mv1=0;}
            	else if (262 == event.getAction()) {
            		moving=true;
                   	pointerCount--;
    				}}
            	
            	float yy = y[0];
            	mv1=x[0];
            	for (int i=1;i<pointerCount; i++){
            		if (yy<y[i]){yy=y[i];mv1=x[i];}}
				if(moving==true){
				
				fr =  D4 + (D4*yy/margen)*(1776.0f/height);
				ajustarfreq(three, 3);
//				llamar();
				}}
            	return true;}});
        
        
        
        four.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            	if (sonando==0||sonando==4){
            		sonando=4;
            		moving = true;
            	decay=false;
            	moving=true;
            	int pointerCount = event.getPointerCount();
            	for (int i = 0; i < 10; i++) {
                    if (i >= pointerCount) continue;
                    
				if (MotionEvent.ACTION_DOWN == event.getAction()) {
					
                	 y[i] = (int) event.getY(i);
                	 contador=0;
                	 y_cord2 = (int) event.getX(i);
                	 
            	
            	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
            		y[i] = (int) event.getY(i);  
            		int y_cord = (int) event.getX(i);
                    x[i] = Math.abs(1-(y_cord-y_cord2));
            	}
            	else if (MotionEvent.ACTION_UP == event.getAction()) {
            		sonando=0;
            		moving=false;
            		four.setBackgroundColor(Color.parseColor("#000000"));
            		decay=true;
            		mv2=mv1=0;}
            	else if (262 == event.getAction()) {
            		moving=true;
                   	pointerCount--;
    				}}
            	
            	float yy = y[0];
            	mv1=x[0];
            	for (int i=1;i<pointerCount; i++){
            		if (yy<y[i]){yy=y[i];mv1=x[i];}}
				if(moving==true){
//				texto.setText("decay: "+moving);
				fr =  G3 + (G3*yy/margen)*(1776.0f/height);
				ajustarfreq(four, 4);
//				llamar();
				}}
            	return true;}});
        
        
        
	}
	
	 
	 
	public void generate(){
		
		
		
		
       audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                                         sr, AudioFormat.CHANNEL_OUT_MONO,
                                 AudioFormat.ENCODING_PCM_16BIT, buffsizeaudio,
                                 AudioTrack.MODE_STREAM);

       
       

       // start audio
      audioTrack.play();

      // synthesis loop
      while(isRunning){
    	  
    	  
    	  
    	  ///////////////////
    	  ///////////////////tremolo
    	  if (moving==true&&mv1>130){
    		
    	  if (bajar1==true)
    		  mv2-=mv1/5000f;
    	  else
    		  mv2+=mv1/5000f;
    	  
    	  
    	  if (mv2>10)
    		  bajar1=true;
    	  if (mv2<-10)
    		  bajar1=false;
    	  }
    	  else{
    		  mv2=mv1=0;
    	  }
    	  
    	  
    	  
    	  //subir
    	  if (contador<1)
    		  contador+=0.0008f;
    	  
    	  
    	  if (decay==false)
    		  ampx=(ampx*1000+5000)/1001;
    	  
    	  if (decay==true){
    		  ampx/=1.0005f;
    	  }
    	  
    	  
    	  
//    	  if (ampx<0)
//    		  ampx=0;

    	  fr2=(fr2*200+fr)/201;
       
       for(int i=0; i < buffsizeaudio; i++){
    	   
    	   

       	nuevo1 = volume1*ampx*contador*Math.sin(ph*freq1*10+des1);
       	nuevo2 = volume2*ampx*contador*Math.sin(ph*freq2*10+des2);
       	nuevo3 = volume3*ampx*contador*Math.sin(ph*freq3*10+des3);
    	   
//    	   osc1 = volume1*ampx*Math.sin((ph)*10+des1);
//    	   osc2 = 0;
//    	   osc3 = 0;
       	
       	//LALA
       	if (mod_osc1==0){osc1 = nuevo1;}
       	else if(mod_osc1==1){//TRIANGLE1
				   
				   if (osc1_old<=0&&nuevo1>=0)
					   osc1 =-volume1*ampx;
					   else
						   osc1 +=freq1*volume1*200;
					   
					   osc1_old=nuevo1;
				   
			   }
			   else if(mod_osc1==2){//SQUARE1
				   
				   if (nuevo1>0)
					   osc1=volume1*ampx;
				   else
					   osc1=-volume1*ampx;
			   }
       	
       	
       	if (mod_osc2==0){osc2 = nuevo2;}
       	else if(mod_osc2==1){//TRIANGLE2
				   
       		if (osc2_old<=0&&nuevo2>=0)
					   osc2 =-volume2*ampx;
					   else
						   osc2 +=freq2*volume2*200;
					   
					   osc2_old=nuevo2;
					   
			   }
			   else if(mod_osc2==2){//SQUARE2
				   
				   if (nuevo2>0)
					   osc2=volume2*ampx;
				   else
					   osc2=-volume2*ampx;
			   }
       	
       	if (mod_osc3==0){osc3 = nuevo3;}
       	else if(mod_osc3==1){//TRIANGLE3
       		if (osc3_old<=0&&nuevo3>=0)
					   osc3 =-volume3*ampx;
					   else
						   osc3 +=freq3*volume3*200;
					   
					   osc3_old=nuevo1;
			   }
			   else if(mod_osc3==2){//SQUARE3
				   
				   if (nuevo3>0)
					   osc3=volume3*ampx;
				   else
					   osc3=-volume3*ampx;
			   }
       	
       	
       	
       	
       	//MODULACIÓN 1			   
			   if (trem1!=0){
			   if (tr_sub1==false)
			   tremolo1-=trem1;
			   else
				   tremolo1+=trem1;
			   if (tremolo1>=0.75f){
				   tr_sub1=false;
				   if (mod_osc1==1)
				   tremolo1=-trem1;
			   }
			   else if (tremolo1<=-0.75f){
				   tr_sub1=true;
				   if (mod_osc1==1)
					   tremolo1=trem1;
			   }
			   osc1*=tremolo1+maxtr1;
			   
			   }
			   
			 //MODULACIÓN 2			   
			   if (trem2!=0){
			   if (tr_sub2==false)
			   tremolo2-=trem2;
			   else
				   tremolo2+=trem2;
			   if (tremolo2>=0.75f){
				   tr_sub2=false;
				   if (mod_osc2==1)
					   tremolo2=-trem2;
			   }
			   else if (tremolo2<=-0.75f){
				   tr_sub2=true;
				   if (mod_osc2==1)
					   tremolo2=trem2;
			   }
			   osc2*=tremolo2+maxtr2;
			   }
			   
			 //MODULACIÓN 3		   
			   if (trem3!=0){
			   if (tr_sub3==false)
			   tremolo3-=trem3;
			   else
				   tremolo3+=trem3;
			   if (tremolo3>=0.75f){
				   tr_sub3=false;
				   if (mod_osc3==1)
					   tremolo3=-trem3;
			   }
			   else if (tremolo3<=-0.75f){
				   tr_sub3=true;
				   if (mod_osc3==1)
					   tremolo3=trem3;
			   }
			   osc3*=tremolo3+maxtr3;
			   }
//       	
			   osc1*=Math.pow(releas1, i);
			   osc2*=Math.pow(releas2, i);
			   osc3*=Math.pow(releas3, i);
//			   
//			   
       	
       	
				   
       	samples[i]=(short)(osc1+osc2+osc3);
       	
         ph += twopi*(fr2+mv2)/sr;
       }
      audioTrack.write(samples, 0, buffsizeaudio);
     }
     audioTrack.stop();
     audioTrack.release();
   
	}
	
	 
	 
	 public void ajustarfreq(RelativeLayout rl, float nota){
		 
		 float rr=255, bb=255, gg=255;
		
		 if (nota==4){
			 nota=0.2f;
			 rr=87;
			 gg=173;
			 bb=206;
		 }
		 else if (nota==3){
			 nota=0.13f;
			 rr=58;
			 gg=191;
			 bb=62;
		 }
		 else if (nota==2){
			 nota=0.09f;
			 rr=198;
			 gg=178;
			 bb=40;
		 }
		 else if (nota==1){
			 nota=0.055f;
			 rr=218;
			 gg=62;
			 bb=81;
		 }
		 
		 float minimo=(float) Math.abs(fr-notas[0]);
		 
		 
		 for (int i=0; i<notas.length; i++){
			 if (Math.abs(fr-notas[i])<minimo){
				 minimo=(float)(Math.abs(fr-notas[i]));
				 seleccionada=i;
			 }
		 }
		 
		 
		 float r=rr-rr*minimo*nota;
		 float b=bb-bb*minimo*nota;
		 float g = gg-gg*minimo*nota;
		 if (r<0)r=0;
		 if (g<0)g=0;
		 if (b<0)b=0;
		 
		 rl.setBackgroundColor(Color.rgb((int)(r),(int)(g), (int)b));
		 
		 
		 fr = (fr*2+notas[seleccionada])/3;
	 }
	 
	 
	
	 
	 
	 @Override
	  public void onWindowFocusChanged(boolean hasFocus) {
	   // TODO Auto-generated method stub
	   super.onWindowFocusChanged(hasFocus);
	   
	   
	   height = getWindowManager().getDefaultDisplay().getHeight();
	   
	   
	 }
	 
	 
	//TOAST
			public void toast(String a) {
				Toast toast = Toast.makeText(getApplicationContext(), a,
						Toast.LENGTH_SHORT);
				toast.show();
				}
			
			
			
			@Override
			 public boolean onKeyDown(int keyCode, KeyEvent event) {
			  switch(keyCode){
			    case KeyEvent.KEYCODE_VOLUME_UP:
			      event.startTracking();
			      return true;
			    case KeyEvent.KEYCODE_VOLUME_DOWN:
			    	event.startTracking();
			      return true;
			    case KeyEvent.KEYCODE_BACK:
			    	this.finish();
			      return true;
			  }
			  return super.onKeyDown(keyCode, event);
			 }
			 
			 
			 @Override
			 public boolean onKeyUp(int keyCode, KeyEvent event) {
			  switch(keyCode){
			    case KeyEvent.KEYCODE_VOLUME_UP:
			    	if(event.isTracking() && !event.isCanceled())
				    	  audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
				                  AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
				        return true;
			    case KeyEvent.KEYCODE_VOLUME_DOWN:
			    	if(event.isTracking() && !event.isCanceled())
				    	  audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
				                  AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
				        return true;
			    }
			    return super.onKeyDown(keyCode, event);
			 }
			 
			 
			 @Override
			 public boolean onKeyLongPress(int keyCode, KeyEvent event) {
				 switch(keyCode){
				    case KeyEvent.KEYCODE_VOLUME_UP:
				    	 Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
			               myIntent.putExtra("volume1", volume1); 
			               myIntent.putExtra("volume2", volume2); 
			               myIntent.putExtra("volume3", volume3); 
			               myIntent.putExtra("des1", des1); 
			               myIntent.putExtra("des2", des2); 
			               myIntent.putExtra("des3", des3); 
			               myIntent.putExtra("releas1", releas1); 
			               myIntent.putExtra("releas2", releas2);
			               myIntent.putExtra("releas3", releas3);
			               myIntent.putExtra("maxtr1", maxtr1); 
			               myIntent.putExtra("maxtr2", maxtr2); 
			               myIntent.putExtra("maxtr3", maxtr3); 
			               myIntent.putExtra("mod_osc1", mod_osc1); 
			               myIntent.putExtra("mod_osc2", mod_osc2); 
			               myIntent.putExtra("mod_osc3", mod_osc3); 
			               myIntent.putExtra("freq1", freq1);
			               myIntent.putExtra("freq2", freq2);
			               myIntent.putExtra("freq3", freq3);
			               myIntent.putExtra("trem1", trem1);
			               myIntent.putExtra("trem2", trem2);
			               myIntent.putExtra("trem3", trem3);
			               myIntent.putExtra("tremolo1", tremolo1);
			               myIntent.putExtra("tremolo2", tremolo2);
			               myIntent.putExtra("tremolo3", tremolo3);
			               myIntent.putExtra("tr_sub1", tr_sub1);
			               myIntent.putExtra("tr_sub2", tr_sub2);
			               myIntent.putExtra("tr_sub3", tr_sub3);
			               
			               this.finish();
			               
			               ampx=0;
			               startActivityForResult(myIntent, 0);
			               
			               
			               
				    	return true;
				    case KeyEvent.KEYCODE_VOLUME_DOWN:
				    	Intent myIntent2 = new Intent(getApplicationContext(), piano.class);
			               myIntent2.putExtra("volume1", volume1); 
			               myIntent2.putExtra("volume2", volume2); 
			               myIntent2.putExtra("volume3", volume3); 
			               myIntent2.putExtra("des1", des1); 
			               myIntent2.putExtra("des2", des2); 
			               myIntent2.putExtra("des3", des3); 
			               myIntent2.putExtra("releas1", releas1); 
			               myIntent2.putExtra("releas2", releas2);
			               myIntent2.putExtra("releas3", releas3);
			               myIntent2.putExtra("maxtr1", maxtr1); 
			               myIntent2.putExtra("maxtr2", maxtr2); 
			               myIntent2.putExtra("maxtr3", maxtr3); 
			               myIntent2.putExtra("mod_osc1", mod_osc1); 
			               myIntent2.putExtra("mod_osc2", mod_osc2); 
			               myIntent2.putExtra("mod_osc3", mod_osc3); 
			               myIntent2.putExtra("freq1", freq1);
			               myIntent2.putExtra("freq2", freq2);
			               myIntent2.putExtra("freq3", freq3);
			               myIntent2.putExtra("trem1", trem1);
			               myIntent2.putExtra("trem2", trem2);
			               myIntent2.putExtra("trem3", trem3);
			               myIntent2.putExtra("tremolo1", tremolo1);
			               myIntent2.putExtra("tremolo2", tremolo2);
			               myIntent2.putExtra("tremolo3", tremolo3);
			               myIntent2.putExtra("tr_sub1", tr_sub1);
			               myIntent2.putExtra("tr_sub2", tr_sub2);
			               myIntent2.putExtra("tr_sub3", tr_sub3);
			               
			               this.finish();
			               
			               ampx=0;
			               startActivityForResult(myIntent2, 0);
				    	return true;
				 }
			  return true;
			 }


			 public void cargar(){
					try {
						File jijio = new File(getApplicationInfo().dataDir, "colors.txt");
						FileInputStream fstream2 = new FileInputStream(jijio);
						DataInputStream in2 = new DataInputStream(fstream2);
						BufferedReader br2 = new BufferedReader(new InputStreamReader(
								in2));

						String linea= br2.readLine();
						
						String []lineas= linea.split(" ");
						c = Integer.parseInt(lineas[0]);
						cs = Integer.parseInt(lineas[1]);
						d = Integer.parseInt(lineas[2]);
						ds = Integer.parseInt(lineas[3]);
						e = Integer.parseInt(lineas[4]);
						f = Integer.parseInt(lineas[5]);
						fs = Integer.parseInt(lineas[6]);
						g = Integer.parseInt(lineas[7]);
						gs = Integer.parseInt(lineas[8]);
						a = Integer.parseInt(lineas[9]);
						as = Integer.parseInt(lineas[10]);
						b = Integer.parseInt(lineas[11]);
						in2.close();
					} catch (Exception e) {
					}
		 }
			
			 
			 public void setColor(RelativeLayout rl, int numero){
				 if (numero==0)
					 rl.setBackgroundColor(Color.parseColor("#FFFFFF"));
		     	
		     	if (numero==1)
		     		rl.setBackgroundColor(Color.parseColor("#AA0000"));
		     	
		     	if (numero==2)
		     		rl.setBackgroundColor(Color.parseColor("#00AA00"));
		     	
		     	if (numero==3)
		     		rl.setBackgroundColor(Color.parseColor("#0000AA"));
			 }
			 
			 
			 @Override
			  public void onStart() {
			    super.onStart();
			    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
			  }

			  @Override
			  public void onStop() {
			    super.onStop();
			    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
			  }

}
