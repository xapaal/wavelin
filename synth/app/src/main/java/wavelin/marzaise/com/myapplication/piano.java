package wavelin.marzaise.com.myapplication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class piano extends Activity {
	
	
	private AudioManager audio;
	
	RelativeLayout doo, re, mi, fa, sol, la, si, dosharp, resharp, fasharp, solsharp, lasharp;
	
	int c=0, cs=0, d=0, ds=0, e=0, f=0, fs=0, g=0, gs=0, a=0, as=0, b=0;
	
   
    int mod_osc1=0, mod_osc2=0, mod_osc3=0;//0>SINE, 1>TRI, 2>SQU
    
    
    float des1, des2, des3;
    float releas1=1, releas2=1, releas3=1;
    float maxtr1, maxtr2, maxtr3;
    boolean tr_sub1, tr_sub2, tr_sub3;
    float tremolo1, tremolo2, tremolo3;
	float volume1=1, volume2=0, volume3=0;
	float freq1=0.1f, freq2=0.1f, freq3=0.1f;
	float trem1=0, trem2=0, trem3=0;
   
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.piano);
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        
        cargar();
        
        
        
        try{
        	Bundle extras = getIntent().getExtras(); 
        	volume1 = extras.getFloat("volume1");
        	volume2 = extras.getFloat("volume2");
        	volume3 = extras.getFloat("volume3");
        	des1 = extras.getFloat("des1");
        	des2 = extras.getFloat("des2");
        	des3 = extras.getFloat("des3");
        	releas1 = extras.getFloat("releas1");
        	releas2 = extras.getFloat("releas2");
        	releas3 = extras.getFloat("releas3");
        	maxtr1 = extras.getFloat("maxtr1");
        	maxtr2 = extras.getFloat("maxtr2");
        	maxtr3 = extras.getFloat("maxtr3");
        	mod_osc1 = extras.getInt("mod_osc1");
        	mod_osc2 = extras.getInt("mod_osc2");
        	mod_osc3 = extras.getInt("mod_osc3");
        	freq1 = extras.getFloat("freq1");
        	freq2 = extras.getFloat("freq2");
        	freq3 = extras.getFloat("freq3");
        	trem1 = extras.getFloat("trem1");
        	trem2 = extras.getFloat("trem2");
        	trem3 = extras.getFloat("trem3");
        	tremolo1 = extras.getFloat("tremolo1");
        	tremolo2 = extras.getFloat("tremolo2");
        	tremolo3 = extras.getFloat("tremolo3");
        	tr_sub1 = extras.getBoolean("tr_sub1");
        	tr_sub2 = extras.getBoolean("tr_sub2");
        	tr_sub3 = extras.getBoolean("tr_sub3");
        }
        catch (Exception E){}
        
        
        
        
        doo = (RelativeLayout) findViewById(R.id.doo);
        re = (RelativeLayout) findViewById(R.id.re);
        mi = (RelativeLayout) findViewById(R.id.mi);
        fa = (RelativeLayout) findViewById(R.id.fa);
        sol = (RelativeLayout) findViewById(R.id.sol);
        la = (RelativeLayout) findViewById(R.id.la);
        si = (RelativeLayout) findViewById(R.id.si);
        
        dosharp = (RelativeLayout) findViewById(R.id.dosharp);
        resharp = (RelativeLayout) findViewById(R.id.resharp);
        fasharp = (RelativeLayout) findViewById(R.id.fasharp);
        solsharp = (RelativeLayout) findViewById(R.id.solsharp);
        lasharp = (RelativeLayout) findViewById(R.id.lasharp);
        
        
        
        setColor(doo, c);
    	setColor(dosharp, cs);
    	setColor(re, d);
    	setColor(resharp, ds);
    	setColor(mi, e);
    	setColor(fa, f);
    	setColor(fasharp, fs);
    	setColor(sol, g);
    	setColor(solsharp, gs);
    	setColor(la, a);
    	setColor(lasharp, as);
    	setColor(si, b);
        
        
        doo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	c++;
            	if (c==4)
            		c=0;
            	setColor(doo, c);
            }
        });
        
        
        dosharp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	cs++;
            	if (cs==4)
            		cs=0;
            	setColor(dosharp, cs);
            }
        });
        
        
        re.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	d++;
            	if (d==4)
            		d=0;
            	setColor(re, d);
            }
        });
        
        
        resharp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	ds++;
            	if (ds==4)
            		ds=0;
            	setColor(resharp, ds);
            }
        });
        
        
        mi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	e++;
            	if (e==4)
            		e=0;
            	setColor(mi, e);
            }
        });
        
        
        fa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	f++;
            	if (f==4)
            		f=0;
            	setColor(fa, f);
            }
        });
        
        fasharp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	fs++;
            	if (fs==4)
            		fs=0;
            	setColor(fasharp, fs);
            }
        });
        
        sol.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	g++;
            	if (g==4)
            		g=0;
            	setColor(sol, g);
            }
        });
        
        
        solsharp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	gs++;
            	if (gs==4)
            		gs=0;
            	setColor(solsharp, gs);
            }
        });
        
        
        la.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	a++;
            	if (a==4)
            		a=0;
            	setColor(la, a);
            }
        });
        
        
        lasharp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	as++;
            	if (as==4)
            		as=0;
            	setColor(lasharp, as);
            }
        });
        
        
        
        si.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	b++;
            	if (b==4)
            		b=0;
            	setColor(si, b);
            }
        });
        
        
        
        
        
        
        
        
	}
	

	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	  switch(keyCode){
	    case KeyEvent.KEYCODE_VOLUME_UP:
	      event.startTracking();
	      return true;
	    case KeyEvent.KEYCODE_VOLUME_DOWN:
	    	event.startTracking();
	      return true;
	    case KeyEvent.KEYCODE_BACK:
	    	this.finish();
	      return true;
	  }
	  return super.onKeyDown(keyCode, event);
	 }

	/*
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	
	 */
	 
	 @Override
	 public boolean onKeyUp(int keyCode, KeyEvent event) {
	  switch(keyCode){
	    case KeyEvent.KEYCODE_VOLUME_UP:
	    	if(event.isTracking() && !event.isCanceled())
		    	  audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
		                  AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
		        return true;
	    case KeyEvent.KEYCODE_VOLUME_DOWN:
	    	if(event.isTracking() && !event.isCanceled())
		    	  audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
		                  AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
		        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	 }
	 
	 
	 @Override
	 public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		 switch(keyCode){
		    case KeyEvent.KEYCODE_VOLUME_UP:
		    	guardar();
		    	 Intent myIntent = new Intent(getApplicationContext(), instrument.class);
	               myIntent.putExtra("volume1", volume1); 
	               myIntent.putExtra("volume2", volume2); 
	               myIntent.putExtra("volume3", volume3); 
	               myIntent.putExtra("des1", des1); 
	               myIntent.putExtra("des2", des2); 
	               myIntent.putExtra("des3", des3); 
	               myIntent.putExtra("releas1", releas1); 
	               myIntent.putExtra("releas2", releas2);
	               myIntent.putExtra("releas3", releas3);
	               myIntent.putExtra("maxtr1", maxtr1); 
	               myIntent.putExtra("maxtr2", maxtr2); 
	               myIntent.putExtra("maxtr3", maxtr3); 
	               myIntent.putExtra("mod_osc1", mod_osc1); 
	               myIntent.putExtra("mod_osc2", mod_osc2); 
	               myIntent.putExtra("mod_osc3", mod_osc3); 
	               myIntent.putExtra("freq1", freq1);
	               myIntent.putExtra("freq2", freq2);
	               myIntent.putExtra("freq3", freq3);
	               myIntent.putExtra("trem1", trem1);
	               myIntent.putExtra("trem2", trem2);
	               myIntent.putExtra("trem3", trem3);
	               myIntent.putExtra("tremolo1", tremolo1);
	               myIntent.putExtra("tremolo2", tremolo2);
	               myIntent.putExtra("tremolo3", tremolo3);
	               myIntent.putExtra("tr_sub1", tr_sub1);
	               myIntent.putExtra("tr_sub2", tr_sub2);
	               myIntent.putExtra("tr_sub3", tr_sub3);
	               
	               this.finish();
	               
	               startActivityForResult(myIntent, 0);
	               
	               
	               
		    	return true;
		    case KeyEvent.KEYCODE_VOLUME_DOWN:
		    	guardar();
		    	Intent myIntent2 = new Intent(getApplicationContext(), MainActivity.class);
	               myIntent2.putExtra("volume1", volume1); 
	               myIntent2.putExtra("volume2", volume2); 
	               myIntent2.putExtra("volume3", volume3); 
	               myIntent2.putExtra("des1", des1); 
	               myIntent2.putExtra("des2", des2); 
	               myIntent2.putExtra("des3", des3); 
	               myIntent2.putExtra("releas1", releas1); 
	               myIntent2.putExtra("releas2", releas2);
	               myIntent2.putExtra("releas3", releas3);
	               myIntent2.putExtra("maxtr1", maxtr1); 
	               myIntent2.putExtra("maxtr2", maxtr2); 
	               myIntent2.putExtra("maxtr3", maxtr3); 
	               myIntent2.putExtra("mod_osc1", mod_osc1); 
	               myIntent2.putExtra("mod_osc2", mod_osc2); 
	               myIntent2.putExtra("mod_osc3", mod_osc3); 
	               myIntent2.putExtra("freq1", freq1);
	               myIntent2.putExtra("freq2", freq2);
	               myIntent2.putExtra("freq3", freq3);
	               myIntent2.putExtra("trem1", trem1);
	               myIntent2.putExtra("trem2", trem2);
	               myIntent2.putExtra("trem3", trem3);
	               myIntent2.putExtra("tremolo1", tremolo1);
	               myIntent2.putExtra("tremolo2", tremolo2);
	               myIntent2.putExtra("tremolo3", tremolo3);
	               myIntent2.putExtra("tr_sub1", tr_sub1);
	               myIntent2.putExtra("tr_sub2", tr_sub2);
	               myIntent2.putExtra("tr_sub3", tr_sub3);
	               
	               this.finish();
	               
	               startActivityForResult(myIntent2, 0);
		    	return true;
		 }
	  return true;
	 }

	 
	 
	 public void cargar(){
				try {
					File jijio = new File(getApplicationInfo().dataDir, "colors.txt");
					FileInputStream fstream2 = new FileInputStream(jijio);
					DataInputStream in2 = new DataInputStream(fstream2);
					BufferedReader br2 = new BufferedReader(new InputStreamReader(
							in2));

					String linea= br2.readLine();
					
					String []lineas= linea.split(" ");
					c = Integer.parseInt(lineas[0]);
					cs = Integer.parseInt(lineas[1]);
					d = Integer.parseInt(lineas[2]);
					ds = Integer.parseInt(lineas[3]);
					e = Integer.parseInt(lineas[4]);
					f = Integer.parseInt(lineas[5]);
					fs = Integer.parseInt(lineas[6]);
					g = Integer.parseInt(lineas[7]);
					gs = Integer.parseInt(lineas[8]);
					a = Integer.parseInt(lineas[9]);
					as = Integer.parseInt(lineas[10]);
					b = Integer.parseInt(lineas[11]);
					in2.close();
				} catch (Exception e) {
				}
	 }
	 
	 
	 public void guardar(){
		 try {
				
				File jijio = new File(getApplicationInfo().dataDir, "colors.txt");
				
				

				FileWriter fstream = new FileWriter(jijio); // this will allow
																	// to append
				BufferedWriter fbw = new BufferedWriter(fstream);

				
				fbw.write(c+" "+cs+" "+d+" "+ds+" "+e+" "+f+" "+fs+" "+g+" "+gs+" "+a+" "+as+" "+b);
				fbw.newLine();
				fbw.close();
			} catch (IOException e) {
			}
			
	 }
	 
	 
	 public void setColor(RelativeLayout rl, int numero){
		 if (numero==0)
			 rl.setBackgroundColor(Color.parseColor("#FFFFFF"));
     	
     	if (numero==1)
     		rl.setBackgroundColor(Color.parseColor("#AA0000"));
     	
     	if (numero==2)
     		rl.setBackgroundColor(Color.parseColor("#00AA00"));
     	
     	if (numero==3)
     		rl.setBackgroundColor(Color.parseColor("#0000AA"));
	 }
	 

	 
	//TOAST
			public void toast(String a) {
				Toast toast = Toast.makeText(getApplicationContext(), a,
						Toast.LENGTH_SHORT);
				toast.show();}
}


