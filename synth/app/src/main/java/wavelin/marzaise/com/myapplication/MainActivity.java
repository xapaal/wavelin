package wavelin.marzaise.com.myapplication;

import android.R.bool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MainActivity extends Activity {

	boolean prin = false;
		AudioTrack audioTrack;
		graph graf = new graph();
		Thread t;
	    int sr = 44100;
	    boolean isRunning = true;
	    SeekBar fSlider;
	    double sliderval;
	    ImageView grafimg;
	    int buffsizeaudio2=0;
	    int buffsizeaudio = 8;
	    int buffsizeimg = 8;
	    short samples[] = new short[buffsizeaudio];
	    int amp = 30;
	    int ampx =5000;
        double twopi = 8.*Math.atan(1.);
        double fr = 440.f;
        double ph = 0.0;
        ImageView main1, main2, main3;
        ImageView f1, f2, f3;
        ImageView tr1, tr2, tr3;
        ImageView tr1b, tr2b, tr3b;
        ImageView release1, release2, release3;
        ImageView desf1, desf2, desf3;
        boolean moving=false;
        float scaleWidth, scaleHeight;
        Bitmap bblack, bred, bblue, bgreen;
        int y_cord2, resultado, scroll_y_old;
        float volume=0;
        RelativeLayout der, izq;
        int escala=16;
        int mod_osc1=0, mod_osc2=0, mod_osc3=0;//0>SINE, 1>TRI, 2>SQU
        double osc1, osc2, osc3;
        double osc11, osc22, osc33;
        TextView texto;
        double osc1_old=0, osc2_old=0, osc3_old=0;
        TextView amp1, amp2, amp3, fre1, fre2, fre3, mod1, mod2, mod3, mod1b, mod2b, mod3b, releaset1, releaset2, releaset3, desft1, desft2, desft3;
        int[]samples2;
        boolean iniciado=false;
        
        private AudioManager audio;

        float cont;
        float des1, des2, des3;
        float releas1, releas2, releas3;
        float maxtr1, maxtr2, maxtr3;
        double nuevo1, nuevo2, nuevo3;
        boolean tr_sub1, tr_sub2, tr_sub3;
        float tremolo1, tremolo2, tremolo3;
    	int width_p, height_p;
    	int potint1, potint2, potint3, potint4, potint5, potint6, potint7, potint8, potint9, potint10, potint11, potint12, potint13, potint14, potint15, potint16, potint17, potint18, potint100;
		protected float volume1=0, volume2=0, volume3=0;
		protected float freq1=0.1f, freq2=0.1f, freq3=0.1f;
		protected float trem1=0, trem2=0, trem3=0;
	    ImageView tipo1, tipo2, tipo3;
	    
	    
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.activity_main);
	        

			/*
	        // Create the interstitial.
	   	    interstitial = new InterstitialAd(this);
	   	    interstitial.setAdUnitId("ca-app-pub-4659156850559411/2066461084");

	   	    // Create ad request.
	   	    adRequest2 = new AdRequest.Builder().build();

	   	    // Begin loading your interstitial.
	   	    interstitial.loadAd(adRequest2);
	   	    
	   	    
	     // Create the adView.
		    adView = new AdView(this);
		    adView.setAdUnitId("ca-app-pub-4659156850559411/8112994689");
		    adView.setAdSize(AdSize.SMART_BANNER);
		    
		    

		    // Lookup your LinearLayout assuming it's been given
		    // the attribute android:id="@+id/mainLayout".
		    LinearLayout layout = (LinearLayout)findViewById(R.id.publi);

		    // Add the adView to it.
		    layout.addView(adView);

		    // Initiate a generic request.
		    adRequest = new AdRequest.Builder().build();

		    // Load the adView with the ad request.
		    adView.loadAd(adRequest);
		    */
	        
	       // point the slider to thwe GUI widget
	        fSlider = (SeekBar) findViewById(R.id.frequency);
	        grafimg = (ImageView) findViewById(R.id.imageView1);
	        main1 = (ImageView) findViewById(R.id.m1);
	        main2 = (ImageView) findViewById(R.id.m2);
	        main3 = (ImageView) findViewById(R.id.m3);
	        f1 = (ImageView) findViewById(R.id.f1);
	        f2 = (ImageView) findViewById(R.id.f2);
	        f3 = (ImageView) findViewById(R.id.f3);
	        der = (RelativeLayout) findViewById(R.id.der);
	        izq = (RelativeLayout) findViewById(R.id.izq);
	        texto = (TextView) findViewById(R.id.textView1);
	        tipo1=(ImageView) findViewById(R.id.tipo1);
	        tipo2=(ImageView) findViewById(R.id.tipo2);
	        tipo3=(ImageView) findViewById(R.id.tipo3);
	        amp1 = (TextView) findViewById(R.id.amp1);
	        amp2= (TextView) findViewById(R.id.amp2);
	        amp3 = (TextView) findViewById(R.id.amp3);
	        fre1 = (TextView) findViewById(R.id.fre1);
	        fre2 = (TextView) findViewById(R.id.fre2);
	        fre3 = (TextView) findViewById(R.id.fre3);
	        tr1 = (ImageView) findViewById(R.id.tr1);
	        tr2 = (ImageView) findViewById(R.id.tr2);
	        tr3 = (ImageView) findViewById(R.id.tr3);
	        mod1 = (TextView) findViewById(R.id.modul1);
	        mod2 = (TextView) findViewById(R.id.modul2);
	        mod3 = (TextView) findViewById(R.id.modul3);
	        tr1b = (ImageView)findViewById(R.id.tr1b);
	        tr2b = (ImageView)findViewById(R.id.tr2b);
	        tr3b = (ImageView)findViewById(R.id.tr3b);
	        mod1b = (TextView) findViewById(R.id.modul1b);
	        mod2b = (TextView) findViewById(R.id.modul2b);
	        mod3b = (TextView) findViewById(R.id.modul3b);
	        release1 = (ImageView) findViewById(R.id.release1);
	        release2 = (ImageView) findViewById(R.id.release2);
	        release3 = (ImageView) findViewById(R.id.release3);
	        releaset1 = (TextView) findViewById(R.id.releaset1);
	        releaset2 = (TextView) findViewById(R.id.releaset2);
	        releaset3 = (TextView) findViewById(R.id.releaset3);
	        desf1 = (ImageView) findViewById(R.id.desf1);
	        desf2 = (ImageView) findViewById(R.id.desf2);
	        desf3 = (ImageView) findViewById(R.id.desf3);
	        desft1 = (TextView) findViewById(R.id.desftt1);	 
	        desft2 = (TextView) findViewById(R.id.desftt2);
	        desft3 = (TextView) findViewById(R.id.desftt3);
	        
			android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
			
			
			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			
//	        scaleWidth  = 0.65f;
//	        scaleHeight = 0.65f;
//	        
//	        width_p = bblack.getWidth();
//	 	    height_p = bblack.getHeight();
	        
	        bblack = BitmapFactory.decodeResource(getResources(),
	 	               R.drawable.pot_black);
	        bred = BitmapFactory.decodeResource(getResources(),
	 	               R.drawable.pot_red);
	        
	        bblue = BitmapFactory.decodeResource(getResources(),
	 	               R.drawable.pot_blue);
	        
	        bgreen = BitmapFactory.decodeResource(getResources(),
	 	               R.drawable.pot_green);
        	
	        cargar();

	        // create a listener for the slider bar;
	        OnSeekBarChangeListener listener = new OnSeekBarChangeListener() {
	          public void onStopTrackingTouch(SeekBar seekBar) { }
	          public void onStartTrackingTouch(SeekBar seekBar) { }
	          public void onProgressChanged(SeekBar seekBar, 
	                                          int progress,
	                                           boolean fromUser) {
	              if(fromUser) sliderval = progress / (double)seekBar.getMax();
	              fr =  440 + 440*sliderval;
	              
	              texto.setText(sliderval+"");
	              
	           }
	        };

	        // set the listener on the slider
	        fSlider.setOnSeekBarChangeListener(listener);

	        // start a new thread to synthesise audio
	        t = new Thread() {
	         public void run() {
	         // set process priority
	         setPriority(Thread.MAX_PRIORITY);
	         // set the buffer size
	        
	        // create an audiotrack object
	         
	         
	        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
	                                          sr, AudioFormat.CHANNEL_OUT_MONO,
	                                  AudioFormat.ENCODING_PCM_16BIT, buffsizeaudio,
	                                  AudioTrack.MODE_STREAM);

	        
	        

	        // start audio
	       audioTrack.play();

	       // synthesis loop
	       while(isRunning){
	        
	        for(int i=0; i < buffsizeaudio; i++){

	        	nuevo1 = volume1*ampx*Math.sin(ph*freq1*10+des1);
	        	nuevo2 = volume2*ampx*Math.sin(ph*freq2*10+des2);
	        	nuevo3 = volume3*ampx*Math.sin(ph*freq3*10+des3);
	        	
	        	//LALA
	        	if (mod_osc1==0){osc1 = nuevo1;}
	        	else if(mod_osc1==1){//TRIANGLE1
					   
					   if (osc1_old<=0&&nuevo1>=0)
						   osc1 =-volume1*ampx;
						   else
							   osc1 +=freq1*volume1*200;
						   
						   osc1_old=nuevo1;
					   
				   }
				   else if(mod_osc1==2){//SQUARE1
					   
					   if (nuevo1>0)
						   osc1=volume1*ampx;
					   else
						   osc1=-volume1*ampx;
				   }
	        	
	        	
	        	if (mod_osc2==0){osc2 = nuevo2;}
	        	else if(mod_osc2==1){//TRIANGLE2
					   
	        		if (osc2_old<=0&&nuevo2>=0)
						   osc2 =-volume2*ampx;
						   else
							   osc2 +=freq2*volume2*200;
						   
						   osc2_old=nuevo2;
						   
				   }
				   else if(mod_osc2==2){//SQUARE2
					   
					   if (nuevo2>0)
						   osc2=volume2*ampx;
					   else
						   osc2=-volume2*ampx;
				   }
	        	
	        	if (mod_osc3==0){osc3 = nuevo3;}
	        	else if(mod_osc3==1){//TRIANGLE3
	        		if (osc3_old<=0&&nuevo3>=0)
						   osc3 =-volume3*ampx;
						   else
							   osc3 +=freq3*volume3*200;
						   
						   osc3_old=nuevo1;
				   }
				   else if(mod_osc3==2){//SQUARE3
					   
					   if (nuevo3>0)
						   osc3=volume3*ampx;
					   else
						   osc3=-volume3*ampx;
				   }
	        	
	        	
	        	
	        	
	        	//MODULACI�N 1			   
				   if (trem1!=0){
				   if (tr_sub1==false)
				   tremolo1-=trem1;
				   else
					   tremolo1+=trem1;
				   if (tremolo1>=0.75f){
					   tr_sub1=false;
					   if (mod_osc1==1)
					   tremolo1=-trem1;
				   }
				   else if (tremolo1<=-0.75f){
					   tr_sub1=true;
					   if (mod_osc1==1)
						   tremolo1=trem1;
				   }
				   osc1*=tremolo1+maxtr1;
				   
				   }
				   
				 //MODULACI�N 2			   
				   if (trem2!=0){
				   if (tr_sub2==false)
				   tremolo2-=trem2;
				   else
					   tremolo2+=trem2;
				   if (tremolo2>=0.75f){
					   tr_sub2=false;
					   if (mod_osc2==1)
						   tremolo2=-trem2;
				   }
				   else if (tremolo2<=-0.75f){
					   tr_sub2=true;
					   if (mod_osc2==1)
						   tremolo2=trem2;
				   }
				   osc2*=tremolo2+maxtr2;
				   }
				   
				 //MODULACI�N 3		   
				   if (trem3!=0){
				   if (tr_sub3==false)
				   tremolo3-=trem3;
				   else
					   tremolo3+=trem3;
				   if (tremolo3>=0.75f){
					   tr_sub3=false;
					   if (mod_osc3==1)
						   tremolo3=-trem3;
				   }
				   else if (tremolo3<=-0.75f){
					   tr_sub3=true;
					   if (mod_osc3==1)
						   tremolo3=trem3;
				   }
				   osc3*=tremolo3+maxtr3;
				   }
	        	
				   osc1*=Math.pow(releas1, i);
				   osc2*=Math.pow(releas2, i);
				   osc3*=Math.pow(releas3, i);
				   
				   
	        	
	        	
					   
	        	samples[i]=(short)(osc1+osc2+osc3);
	        	
	          ph += twopi*fr/sr;
	        }
	       audioTrack.write(samples, 0, buffsizeaudio);
	      }
	      audioTrack.stop();
	      audioTrack.release();
	    }
	   };

	   t.start();
	   
	   tipo1.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v){
        	   mod_osc1++;
        	   if (mod_osc1>2)
        		   mod_osc1=0;
        	   
        	   if (mod_osc1==0)tipo1.setImageDrawable(getResources().getDrawable(R.drawable.sine));
        	   if (mod_osc1==1)tipo1.setImageDrawable(getResources().getDrawable(R.drawable.triangular));
        	   if (mod_osc1==2)tipo1.setImageDrawable(getResources().getDrawable(R.drawable.square));
        	   
        	   actualizarGraf();
           }
       });
	   
	   tipo2.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v){
        	   mod_osc2++;
        	   if (mod_osc2>2)
        		   mod_osc2=0;
        	   
        	   if (mod_osc2==0)tipo2.setImageDrawable(getResources().getDrawable(R.drawable.sine));
        	   if (mod_osc2==1)tipo2.setImageDrawable(getResources().getDrawable(R.drawable.triangular));
        	   if (mod_osc2==2)tipo2.setImageDrawable(getResources().getDrawable(R.drawable.square));
        	   
        	   actualizarGraf();
           }
       });
	   
	   tipo3.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v){
        	   mod_osc3++;
        	   if (mod_osc3>2)
        		   mod_osc3=0;
        	   
        	   if (mod_osc3==0)tipo3.setImageDrawable(getResources().getDrawable(R.drawable.sine));
        	   if (mod_osc3==1)tipo3.setImageDrawable(getResources().getDrawable(R.drawable.triangular));
        	   if (mod_osc3==2)tipo3.setImageDrawable(getResources().getDrawable(R.drawable.square));
        	   
        	   actualizarGraf();
           }
       });
	   
       izq.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v){
               escala/=2;
               if (escala<1){
            	   escala=1;
               }
               samples2=new int[buffsizeimg*escala];
               graf.actbufferescala(buffsizeimg*escala);
               actualizarGraf();
           }
       });
       
       der.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v){
               escala*=2;
               if (escala>128){
            	   escala=128;
               }
               samples2=new int[buffsizeimg*escala];
               graf.actbufferescala(buffsizeimg*escala);
               actualizarGraf();
           }
       });
	   
	   
	   main1.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {
           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
               	 y_cord2 = (int) event.getRawY();
           	moving = true;
           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
                        int y_cord = (int) event.getRawY();
                        int scroll_y = 1-(y_cord-y_cord2);
                        if (scroll_y==scroll_y_old){resultado=0;}
                        else
                       	 {resultado=scroll_y/7;
                        scroll_y_old=scroll_y;}
                        potint1+=resultado;
                        
                        if (potint1>160){potint1=160;}
                        if (potint1<-160){potint1=-160;}
                        
                         volume1 = (1/320f)*potint1+1/2f; 
        			    moving = false;
           	}
        		
           	else if (MotionEvent.ACTION_UP == event.getAction()) {
           		if (moving){
                   
                   	if (potint1<-60){
   		    			potint1=160;
   		    			volume1=1;
   		    		}
   		    		else{
   		    			potint1=-160;
   		    			volume1=0;
   		    		}
                   	
                   }
           	}
           	rotar(main1,potint1, bblack);
//           	try{SM.changeVolume(volume1, streambase);}catch (Exception E){}
           	amp1.setText("amp: "+(int)(volume1*100));
           	actualizarGraf();
           	return true;
           }
    });
	    
	    
	    main2.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint2+=resultado;
	                        
	                        if (potint2>160){potint2=160;}
	                        if (potint2<-160){potint2=-160;}
	                        
	                         volume2 = (1/320f)*potint2+1/2f; 
	        			    moving = false;
	           	}
	        		
	           	else if (MotionEvent.ACTION_UP == event.getAction()) {
	           		if (moving){
	                   
	                   	if (potint2<-60){
	   		    			potint2=160;
	   		    			volume2=1;
	   		    		}
	   		    		else{
	   		    			potint2=-160;
	   		    			volume2=0;
	   		    		}
	                   	
	                   }
	           	}
	           	rotar(main2,potint2, bblack);
//	           	try{SM.changeVolume(volume1, streambase);}catch (Exception E){}
 	           amp2.setText("amp: "+(int)(volume2*100));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    main3.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint5+=resultado;
	                        
	                        if (potint5>160){potint5=160;}
	                        if (potint5<-160){potint5=-160;}
	                        
	                         volume3 = (1/320f)*potint5+1/2f; 
	        			    moving = false;
	           	}
	        		
	           	else if (MotionEvent.ACTION_UP == event.getAction()) {
	           		if (moving){
	                   
	                   	if (potint5<-60){
	   		    			potint5=160;
	   		    			volume3=1;
	   		    		}
	   		    		else{
	   		    			potint5=-160;
	   		    			volume3=0;
	   		    		}
	                   	
	                   }
	           	}
	           	rotar(main3,potint5, bblack);
//	           	try{SM.changeVolume(volume1, streambase);}catch (Exception E){}
	           	amp3.setText("amp: "+(int)(volume3*100));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    f1.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint3+=resultado;
	                        
	                        if (potint3>160){potint3=160;}
	                        if (potint3<-160){potint3=-160;}
	                        
	                         freq1 = (1/400f)*potint3+0.5f; 
	        			    moving = false;
	           	}
	           	rotar(f1,potint3, bred);
//	           	try{SM.changeVolume(volume1, streambase);}catch (Exception E){}
	           	//puta
	           	fre1.setText((int)(freq1*4400)+" Hz");
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    f2.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint4+=resultado;
	                        
	                        if (potint4>160){potint4=160;}
	                        if (potint4<-160){potint4=-160;}
	                        
	                         freq2 = (1/400f)*potint4+0.5f; 
	        			    moving = false;
	           	}
	           	rotar(f2,potint4, bred);
//	           	try{SM.changeVolume(volume1, streambase);}catch (Exception E){}
	           	fre2.setText((int)(freq2*4400)+" Hz");
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    f3.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint6+=resultado;
	                        
	                        if (potint6>160){potint6=160;}
	                        if (potint6<-160){potint6=-160;}
	                        
	                         freq3 = (1/400f)*potint6+0.5f; 
	        			    moving = false;
	           	}
	           	rotar(f3,potint6, bred);
//	           	try{SM.changeVolume(volume1, streambase);}catch (Exception E){}
	           	fre3.setText((int)(freq3*4400)+" Hz");
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    tr1.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint7+=resultado;
	                        
	                        if (potint7>160){potint7=160;}
	                        if (potint7<-160){potint7=-160;}
	                        
	                         trem1 = (1/320f)*potint7+1/2f; 
//	                         trem1*=1.05;
	        			    moving = false;
	           	}
	           	rotar(tr1,potint7, bblue);
	           	mod1.setText("mod: "+(int)(trem1*100));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    tr2.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint8+=resultado;
	                        
	                        if (potint8>160){potint8=160;}
	                        if (potint8<-160){potint8=-160;}
	                        
	                         trem2 = (1/320f)*potint8+1/2f; 
//	                         trem1*=1.05;
	        			    moving = false;
	           	}
	           	rotar(tr2,potint8, bblue);
	           	mod2.setText("mod: "+(int)(trem2*100));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    tr3.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint9+=resultado;
	                        
	                        if (potint9>160){potint9=160;}
	                        if (potint9<-160){potint9=-160;}
	                        
	                         trem3 = (1/320f)*potint9+1/2f; 
//	                         trem1*=1.05;
	        			    moving = false;
	           	}
	           	rotar(tr3,potint9, bblue);
	           	mod3.setText("mod: "+(int)(trem3*100));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    tr1b.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint10+=resultado;
	                        
	                        if (potint10>160){potint10=160;}
	                        if (potint10<-160){potint10=-160;}
	                        
	                         maxtr1 = (1/320f)*potint10+1/2f; 
//	                         trem1*=1.05;
	        			    moving = false;
	           	}
	           	rotar(tr1b,potint10, bblue);
	           	mod1b.setText("m.amp: "+(int)(maxtr1*100));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    tr2b.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint11+=resultado;
	                        
	                        if (potint11>160){potint11=160;}
	                        if (potint11<-160){potint11=-160;}
	                        
	                         maxtr2 = (1/320f)*potint11+1/2f; 
//	                         trem1*=1.05;
	        			    moving = false;
	           	}
	           	rotar(tr2b,potint11, bblue);
	           	mod2b.setText("m.amp: "+(int)(maxtr2*100));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    tr3b.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint12+=resultado;
	                        
	                        if (potint12>160){potint12=160;}
	                        if (potint12<-160){potint12=-160;}
	                        
	                         maxtr3 = (1/320f)*potint12+1/2f; 
//	                         trem1*=1.05;
	        			    moving = false;
	           	}
	           	rotar(tr3b,potint12, bblue);
	           	mod3b.setText("m.amp: "+(int)(maxtr3*100));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    
	    release1.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint13+=resultado;
	                        
	                        if (potint13>160){potint13=160;}
	                        if (potint13<-160){potint13=-160;}
	                        
	                         releas1 = (-0.000001f)*potint13+0.99984f; 
	        			    moving = false;
	           	}
	           	
	           	rotar(release1,potint13, bgreen);
	           	releaset1.setText("rel: "+(int)(-releas1*315000+315000));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    
	    release2.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint14+=resultado;
	                        
	                        if (potint14>160){potint14=160;}
	                        if (potint14<-160){potint14=-160;}
	                        
	                         releas2 = (-0.000001f)*potint14+0.99984f; 
//	                         trem1*=1.05;
	        			    moving = false;
	           	}
	           	rotar(release2,potint14, bgreen);
	           	releaset2.setText("rel: "+(int)(-releas2*315000+315000));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    release3.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint15+=resultado;
	                        
	                        if (potint15>160){potint15=160;}
	                        if (potint15<-160){potint15=-160;}
	                        
	                         releas3 = (-0.000001f)*potint15+0.99984f; 
//	                         trem1*=1.05;
	        			    moving = false;
	           	}
	           	rotar(release3,potint15, bgreen);
	           	releaset3.setText("rel: "+(int)(-releas3*315000+315000));
	           	actualizarGraf();
	           	return true;
	           }
	    });
	    
	    
	    
	    
	    desf1.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint16+=resultado;
	                        
	                        if (potint16>160){potint16=160;}
	                        if (potint16<-160){potint16=-160;}
	                        
	                         des1 = potint16/51.0f; 
	        			    moving = false;
	           	}
	        		
	           	else if (MotionEvent.ACTION_UP == event.getAction()) {
	           		if (moving){
	                   		potint16=0;
	                   		des1=0;
	            }}
	           	rotar(desf1,potint16, bgreen);
	           	desft1.setText("pha: "+(int)(des1*28.6875f)+"�");
	           	actualizarGraf();
	           	return true;
	           }});
	    
	    
	    
	    desf2.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint17+=resultado;
	                        
	                        if (potint17>160){potint17=160;}
	                        if (potint17<-160){potint17=-160;}
	                        
	                         des2 = potint17/51.0f; 
	        			    moving = false;
	           	}
	        		
	           	else if (MotionEvent.ACTION_UP == event.getAction()) {
	           		if (moving){
	                   		potint17=0;
	                   		des2=0;
	            }}
	           	rotar(desf2,potint17, bgreen);
	           	desft2.setText("pha: "+(int)(des2*28.6875f)+"�");
	           	actualizarGraf();
	           	return true;
	           }});
	    
	    
	    
	    desf3.setOnTouchListener(new View.OnTouchListener() {
	    	 @Override
	           public boolean onTouch(View v, MotionEvent event) {
	           	if (MotionEvent.ACTION_DOWN == event.getAction()) {
	               	 y_cord2 = (int) event.getRawY();
	           	moving = true;
	           	}else if (MotionEvent.ACTION_MOVE == event.getAction()) {
	                        int y_cord = (int) event.getRawY();
	                        int scroll_y = 1-(y_cord-y_cord2);
	                        if (scroll_y==scroll_y_old){resultado=0;}
	                        else
	                       	 {resultado=scroll_y/7;
	                        scroll_y_old=scroll_y;}
	                        potint18+=resultado;
	                        
	                        if (potint18>160){potint18=160;}
	                        if (potint18<-160){potint18=-160;}
	                        
	                         des3 = potint18/51.0f; 
	        			    moving = false;
	           	}
	        		
	           	else if (MotionEvent.ACTION_UP == event.getAction()) {
	           		if (moving){
	                   		potint18=0;
	                   		des3=0;
	            }}
	           	rotar(desf3,potint18, bgreen);
	           	desft3.setText("pha: "+(int)(des3*28.6875f)+"�");
	           	actualizarGraf();
	           	return true;
	           }});
	    
	    
	    
	    }
	    
	    
	    
	    
	    
	    
	    public void rotar(ImageView im, int degree, Bitmap bmp){
			  Matrix matrix = new Matrix();
		        matrix.postScale(scaleWidth, scaleHeight);
		        
		        
		        matrix.postRotate(degree);
		        Bitmap resizedBitmap = Bitmap.createBitmap(bmp, 0, 0,
		                          width_p, height_p, matrix, true);
		        BitmapDrawable bmd = new BitmapDrawable(resizedBitmap);
		        im.setImageDrawable(bmd);
		        im.setScaleType(ScaleType.CENTER);
		  }

	    

	    public void onDestroy(){
	          super.onDestroy();
	          //displayInterstitial();
	          isRunning = false;
	          try {
	            t.join();
	           } catch (InterruptedException e) {
	             e.printStackTrace();
	           }
	           t = null;
	     }
	    
	    
	    
	  
	  //TOAST
		public void toast(String a) {
			Toast toast = Toast.makeText(getApplicationContext(), a,
					Toast.LENGTH_SHORT);
			toast.show();}
		
		
		public void cargar(){
			
			int height = getWindowManager().getDefaultDisplay().getHeight();
//			   int he = main1.getHeight();
//			   int hee = height/he;
//			   
//			   toast(height+"/"+he+"="+hee);
			
			float caca = height/2732.3f;
			   
			   scaleWidth  = caca;
		        scaleHeight = caca;
			   
			   main1.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   main1.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   main2.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   main2.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   main3.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   main3.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   
			   f1.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   f1.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   f2.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   f2.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   f3.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   f3.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   
			   tr1.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   tr1.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   tr2.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   tr2.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   tr3.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   tr3.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   
			   release1.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   release1.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   release2.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   release2.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   release3.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   release3.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   
			   desf1.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   desf1.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   desf2.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   desf2.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   desf3.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   desf3.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   
			   tr1b.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   tr1b.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   tr2b.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   tr2b.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   tr3b.getLayoutParams().height = (int)(height/14.8f);//nube1 h
			   tr3b.getLayoutParams().width = (int)(height/14.8f);//nube1 h
			   
			   tipo1.getLayoutParams().height = (int)(height/20f);//nube1 h
			   tipo2.getLayoutParams().height = (int)(height/20f);//nube1 h
			   tipo3.getLayoutParams().height = (int)(height/20f);//nube1 h
		}
		@Override
		  public void onWindowFocusChanged(boolean hasFocus) {
		   // TODO Auto-generated method stub
		   super.onWindowFocusChanged(hasFocus);
		   
		   
		   
		   
		   
		   LinearLayout deiz = (LinearLayout) findViewById(R.id.deiz);
		   RelativeLayout nada = (RelativeLayout) findViewById(R.id.nada);
		   
		   float lo = grafimg.getHeight()+nada.getHeight()-10;
		   
		   grafimg.getLayoutParams().height = (int)(lo);//nube1 h
		   deiz.getLayoutParams().height = (int)(lo);//nube1 h
		   
		   
	        
	        width_p = bblack.getWidth();
	 	    height_p = bblack.getHeight();
		   
		   prin = true;
		   
		   if (iniciado==false){
			   
		   potint1 = potint2 = potint3 = potint4 = potint5 = potint6 = potint7 = potint8 = potint9 = -160;
		   potint10 = potint11 = potint12 = potint13 = potint14 = potint15 = -160;
		   potint16 = potint17 = potint18 = 0;
		   
		   tremolo1=0;
		   tremolo2=0;
		   tremolo3=0;
		   maxtr1=0;
		   maxtr2=0;
		   maxtr3=0;
		   
		   releas1=releas2=releas3=1;
		   
		   
		   try{
			   //nuevoo
	        	Bundle extras = getIntent().getExtras(); 
	        	volume1 = extras.getFloat("volume1");potint1=(int)(volume1*320-160);
	        	volume2 = extras.getFloat("volume2");potint2=(int)(volume2*320-160);
	        	volume3 = extras.getFloat("volume3");potint5=(int)(volume3*320-160);
	        	des1 = extras.getFloat("des1"); potint16=(int)(des1*(51));
	        	des2 = extras.getFloat("des2"); potint17=(int)(des2*(51));
	        	des3 = extras.getFloat("des3"); potint18=(int)(des3*(51));
	        	releas1 = extras.getFloat("releas1"); potint13=(int)(releas1*(-1066660)+1066500);
	        	releas2 = extras.getFloat("releas2"); potint14=(int)(releas2*(-1066660)+1066500);
	        	releas3 = extras.getFloat("releas3"); potint15=(int)(releas3*(-1066660)+1066500);
	        	maxtr1 = extras.getFloat("maxtr1"); potint10=(int)(maxtr1*320-160);
	        	maxtr2 = extras.getFloat("maxtr2"); potint11=(int)(maxtr2*320-160);
	        	maxtr3 = extras.getFloat("maxtr3"); potint12=(int)(maxtr3*320-160);
	        	mod_osc1 = extras.getInt("mod_osc1");
	        	mod_osc2 = extras.getInt("mod_osc2");
	        	mod_osc3 = extras.getInt("mod_osc3");
	        	freq1 = extras.getFloat("freq1");potint3=(int)(freq1*400-200);
	        	freq2 = extras.getFloat("freq2");potint4=(int)(freq2*400-200);
	        	freq3 = extras.getFloat("freq3");potint6=(int)(freq3*400-200);
	        	trem1 = extras.getFloat("trem1");potint7=(int)(trem1*320-160);
	        	trem2 = extras.getFloat("trem2");potint8=(int)(trem2*320-160);
	        	trem3 = extras.getFloat("trem3");potint9=(int)(trem3*320-160);
	        	tremolo1 = extras.getFloat("tremolo1");
	        	tremolo2 = extras.getFloat("tremolo2");
	        	tremolo3 = extras.getFloat("tremolo3");
	        	tr_sub1 = extras.getBoolean("tr_sub1");
	        	tr_sub2 = extras.getBoolean("tr_sub2");
	        	tr_sub3 = extras.getBoolean("tr_sub3");
	        }
	        catch (Exception E){}
		   
		   amp1.setText("amp: "+(int)(volume1*100));
		   amp2.setText("amp: "+(int)(volume2*100));
		   amp3.setText("amp: "+(int)(volume3*100));
		   
		   fre1.setText((int)(freq1*4400)+" Hz");
		   fre2.setText((int)(freq2*4400)+" Hz");
		   fre3.setText((int)(freq3*4400)+" Hz");
		   
		   desft1.setText("pha: "+(int)(des1*28.6875f)+"�");
		   desft2.setText("pha: "+(int)(des2*28.6875f)+"�");
		   desft3.setText("pha: "+(int)(des3*28.6875f)+"�");
		   
		   releaset1.setText("rel: "+(int)(-releas1*315000+315000));
		   releaset2.setText("rel: "+(int)(-releas2*315000+315000));
		   releaset3.setText("rel: "+(int)(-releas3*315000+315000));
		   
		   mod1b.setText("m.amp: "+(int)(maxtr1*100));
		   mod2b.setText("m.amp: "+(int)(maxtr2*100));
		   mod3b.setText("m.amp: "+(int)(maxtr3*100));
		   
		   mod1.setText("mod: "+(int)(trem1*100));
		   mod2.setText("mod: "+(int)(trem2*100));
		   mod3.setText("mod: "+(int)(trem3*100));
		   
		   if (mod_osc1==0)tipo1.setImageDrawable(getResources().getDrawable(R.drawable.sine));
    	   if (mod_osc1==1)tipo1.setImageDrawable(getResources().getDrawable(R.drawable.triangular));
    	   if (mod_osc1==2)tipo1.setImageDrawable(getResources().getDrawable(R.drawable.square));
		   
    	   if (mod_osc2==0)tipo2.setImageDrawable(getResources().getDrawable(R.drawable.sine));
    	   if (mod_osc2==1)tipo2.setImageDrawable(getResources().getDrawable(R.drawable.triangular));
    	   if (mod_osc2==2)tipo2.setImageDrawable(getResources().getDrawable(R.drawable.square));
    	   
    	   if (mod_osc3==0)tipo3.setImageDrawable(getResources().getDrawable(R.drawable.sine));
    	   if (mod_osc3==1)tipo3.setImageDrawable(getResources().getDrawable(R.drawable.triangular));
    	   if (mod_osc3==2)tipo3.setImageDrawable(getResources().getDrawable(R.drawable.square));
		   
		   rotar(f1,potint3, bred);
		   rotar(f2,potint4, bred);
		   rotar(f3,potint6, bred);
		   rotar(main1,potint1, bblack);
		   rotar(main2,potint2, bblack);
		   rotar(main3,potint5, bblack);
		   
		   rotar(tr1,potint7, bblue);
		   rotar(tr2,potint8, bblue);
		   rotar(tr3,potint9, bblue);

		   rotar(release1,potint10, bgreen);
		   rotar(release1,potint11, bgreen);
		   rotar(release1,potint12, bgreen);

		   rotar(release1,potint13, bgreen);
		   rotar(release2,potint14, bgreen);
		   rotar(release3,potint15, bgreen);
		   
		   rotar(desf1,potint16, bgreen);
		   rotar(desf2,potint17, bgreen);
		   rotar(desf3,potint18, bgreen);
		   
		   rotar(tr1b, potint10, bblue);
		   rotar(tr2b, potint11, bblue);
		   rotar(tr3b, potint12, bblue);

		   
		   
		   
		   samples2=new int[buffsizeimg*escala];
		   graf.actbufferescala(buffsizeimg*escala);
		   graf.cargar(getApplicationContext(), grafimg.getHeight(), grafimg.getWidth());
//		   grafimg.setImageBitmap(graph.getBMP(samples2, buffsize*escala));
		   
		   
		   actualizarGraf();
		
		   iniciado=true;
		   
		   }
		  }
		
		
		
		
		
		
		
		
		
		
		
		
		
		public void actualizarGraf(){
			
			   tremolo1=0;
			   tremolo2=0;
			   tremolo3=0;
			   tr_sub1=true;
			   tr_sub2=true;
			   tr_sub3=true;
			   
			   cont = 0;
			   for(int i=0; i < buffsizeimg*escala; i++){
				   
				   nuevo1=volume1*amp*Math.sin(cont*freq1*10+des1);
				   nuevo2=volume2*amp*Math.sin(cont*freq2*10+des2);
				   nuevo3=volume3*amp*Math.sin(cont*freq3*10+des3);
				   
//			
				   
				   //LALA
				   
				   if (mod_osc1==0){osc11 =nuevo1;}
				   else if(mod_osc1==1){//TRIANGLE1

					   
					   if (osc1_old<=0&&nuevo1>=0)
					   osc11 =-volume1*amp;
					   else
						   osc11 +=freq1*volume1*(amp/5);
					   
					   osc1_old=nuevo1;
				   }
				   else if(mod_osc1==2){//SQUARE
					   
					   if (nuevo1>0)
						   osc11=volume1*amp;
					   else
						   osc11=-volume1*amp;
					 
				   }
				   
				   
				   if (mod_osc2==0){osc22 =nuevo2;}
				   else if(mod_osc2==1){//TRIANGLE2
					   if (osc2_old<=0&&nuevo2>=0)
						   osc22 =-volume2*amp;
						   else
							   osc22 +=freq2*volume2*(amp/5);
						   
						   osc2_old=nuevo2;
				   }
				   else if(mod_osc2==2){//SQUARE
					   
					   if (nuevo2>0)
						   osc22=volume2*amp;
					   else
						   osc22=-volume2*amp;
					   
				   }
				   
				   if (mod_osc3==0){osc33 =nuevo3;}
				   else if(mod_osc3==1){//TRIANGLE3
					   if (osc3_old<=0&&nuevo3>=0)
						   osc33 =-volume3*amp;
						   else
							   osc33 +=freq3*volume3*(amp/5);
						   
						   osc3_old=nuevo3;
					   
				   }
				   else if(mod_osc3==2){//SQUARE
					   
					   if (nuevo3>0)
						   osc33=volume3*amp;
					   else
						   osc33=-volume3*amp;
					  
				   }
				   
				   
				   
				   
				   
				 //MODULACI�N 1			   
				   if (trem1!=0){
				   if (tr_sub1==false)
				   tremolo1-=trem1;
				   else
					   tremolo1+=trem1;
				   if (tremolo1>=0.75f){
					   tr_sub1=false;
					   if (mod_osc1==1)
					   tremolo1=-trem1;
				   }
				   else if (tremolo1<=-0.75f){
					   tr_sub1=true;
					   if (mod_osc1==1)
						   tremolo1=trem1;
				   }
				   osc11*=(tremolo1+maxtr1);
				   }
				   
				 //MODULACI�N 2			   
				   if (trem2!=0){
				   if (tr_sub2==false)
				   tremolo2-=trem2;
				   else
					   tremolo2+=trem2;
				   if (tremolo2>=0.75f){
					   tr_sub2=false;
					   if (mod_osc2==1)
						   tremolo2=-trem2;
				   }
				   else if (tremolo2<=-0.75f){
					   tr_sub2=true;
					   if (mod_osc2==1)
						   tremolo2=trem2;
				   }
				   osc22*=tremolo2+maxtr2;
				   }
				   
				 //MODULACI�N 3		   
				   if (trem3!=0){
				   if (tr_sub3==false)
				   tremolo3-=trem3;
				   else
					   tremolo3+=trem3;
				   if (tremolo3>=0.75f){
					   tr_sub3=false;
					   if (mod_osc3==1)
						   tremolo3=-trem3;
				   }
				   else if (tremolo3<=-0.75f){
					   tr_sub3=true;
					   if (mod_osc3==1)
						   tremolo3=trem3;
				   }
				   osc33*=tremolo3+maxtr3;
				   }
				   
				 
				   osc11*=Math.pow(releas1, i*10);
				   osc22*=Math.pow(releas2, i*10);
				   osc33*=Math.pow(releas3, i*10);
				   
				   
			        samples2[i]=(int)(osc11+osc22+osc33);
			        cont+=0.0625f;
			    }
			
			 graph graf = new graph();
			   
			   grafimg.setImageBitmap(graph.getBMP(samples2));
			   
		}
		
		void modulacion(float trem, boolean tr_sub, float tremolo, double osc){
			
			
			
			
		}
		
		@Override
		  public void onResume() {
		    super.onResume();
		    
		    
		    ampx=5000;
		    
		    
		    
		    }
			
		
		@Override
		 public boolean onKeyDown(int keyCode, KeyEvent event) {
		  switch(keyCode){
		    case KeyEvent.KEYCODE_VOLUME_UP:
		      event.startTracking();
		      return true;
		    case KeyEvent.KEYCODE_VOLUME_DOWN:
		    	event.startTracking();
		      return true;
		    case KeyEvent.KEYCODE_BACK:
		    	this.finish();
		      return true;
		  }
		  return super.onKeyDown(keyCode, event);
		 }
		 
		 
		 @Override
		 public boolean onKeyUp(int keyCode, KeyEvent event) {
		  switch(keyCode){
		    case KeyEvent.KEYCODE_VOLUME_UP:
		    	if(event.isTracking() && !event.isCanceled())
			    	  audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
			                  AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
			        return true;
		    case KeyEvent.KEYCODE_VOLUME_DOWN:
		    	if(event.isTracking() && !event.isCanceled())
			    	  audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
			                  AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
			        return true;
			        
		    case KeyEvent.KEYCODE_BACK:
		    	
		    	 Intent myIntent = new Intent(getApplicationContext(), instrument.class);
	               myIntent.putExtra("volume1", volume1); 
	               myIntent.putExtra("volume2", volume2); 
	               myIntent.putExtra("volume3", volume3); 
	               myIntent.putExtra("des1", des1); 
	               myIntent.putExtra("des2", des2); 
	               myIntent.putExtra("des3", des3); 
	               myIntent.putExtra("releas1", releas1); 
	               myIntent.putExtra("releas2", releas2);
	               myIntent.putExtra("releas3", releas3);
	               myIntent.putExtra("maxtr1", maxtr1); 
	               myIntent.putExtra("maxtr2", maxtr2); 
	               myIntent.putExtra("maxtr3", maxtr3); 
	               myIntent.putExtra("mod_osc1", mod_osc1); 
	               myIntent.putExtra("mod_osc2", mod_osc2); 
	               myIntent.putExtra("mod_osc3", mod_osc3); 
	               myIntent.putExtra("freq1", freq1);
	               myIntent.putExtra("freq2", freq2);
	               myIntent.putExtra("freq3", freq3);
	               myIntent.putExtra("trem1", trem1);
	               myIntent.putExtra("trem2", trem2);
	               myIntent.putExtra("trem3", trem3);
	               myIntent.putExtra("tremolo1", tremolo1);
	               myIntent.putExtra("tremolo2", tremolo2);
	               myIntent.putExtra("tremolo3", tremolo3);
	               myIntent.putExtra("tr_sub1", tr_sub1);
	               myIntent.putExtra("tr_sub2", tr_sub2);
	               myIntent.putExtra("tr_sub3", tr_sub3);
	               ampx=0;
	               this.finish();
	               startActivityForResult(myIntent, 0);
		    	
	               
			        return true;
		    }
		    return super.onKeyDown(keyCode, event);
		 }
		 
		 
		 @Override
		 public boolean onKeyLongPress(int keyCode, KeyEvent event) {
			 switch(keyCode){
			    case KeyEvent.KEYCODE_VOLUME_UP:
			    	 Intent myIntent = new Intent(getApplicationContext(), piano.class);
		               myIntent.putExtra("volume1", volume1); 
		               myIntent.putExtra("volume2", volume2); 
		               myIntent.putExtra("volume3", volume3); 
		               myIntent.putExtra("des1", des1); 
		               myIntent.putExtra("des2", des2); 
		               myIntent.putExtra("des3", des3); 
		               myIntent.putExtra("releas1", releas1); 
		               myIntent.putExtra("releas2", releas2);
		               myIntent.putExtra("releas3", releas3);
		               myIntent.putExtra("maxtr1", maxtr1); 
		               myIntent.putExtra("maxtr2", maxtr2); 
		               myIntent.putExtra("maxtr3", maxtr3); 
		               myIntent.putExtra("mod_osc1", mod_osc1); 
		               myIntent.putExtra("mod_osc2", mod_osc2); 
		               myIntent.putExtra("mod_osc3", mod_osc3); 
		               myIntent.putExtra("freq1", freq1);
		               myIntent.putExtra("freq2", freq2);
		               myIntent.putExtra("freq3", freq3);
		               myIntent.putExtra("trem1", trem1);
		               myIntent.putExtra("trem2", trem2);
		               myIntent.putExtra("trem3", trem3);
		               myIntent.putExtra("tremolo1", tremolo1);
		               myIntent.putExtra("tremolo2", tremolo2);
		               myIntent.putExtra("tremolo3", tremolo3);
		               myIntent.putExtra("tr_sub1", tr_sub1);
		               myIntent.putExtra("tr_sub2", tr_sub2);
		               myIntent.putExtra("tr_sub3", tr_sub3);
		               this.finish();
		               ampx=0;
		               startActivityForResult(myIntent, 0);
			    	return true;
			    case KeyEvent.KEYCODE_VOLUME_DOWN:
			    	Intent myIntent2 = new Intent(getApplicationContext(), instrument.class);
		               myIntent2.putExtra("volume1", volume1); 
		               myIntent2.putExtra("volume2", volume2); 
		               myIntent2.putExtra("volume3", volume3); 
		               myIntent2.putExtra("des1", des1); 
		               myIntent2.putExtra("des2", des2); 
		               myIntent2.putExtra("des3", des3); 
		               myIntent2.putExtra("releas1", releas1); 
		               myIntent2.putExtra("releas2", releas2);
		               myIntent2.putExtra("releas3", releas3);
		               myIntent2.putExtra("maxtr1", maxtr1); 
		               myIntent2.putExtra("maxtr2", maxtr2); 
		               myIntent2.putExtra("maxtr3", maxtr3); 
		               myIntent2.putExtra("mod_osc1", mod_osc1); 
		               myIntent2.putExtra("mod_osc2", mod_osc2); 
		               myIntent2.putExtra("mod_osc3", mod_osc3); 
		               myIntent2.putExtra("freq1", freq1);
		               myIntent2.putExtra("freq2", freq2);
		               myIntent2.putExtra("freq3", freq3);
		               myIntent2.putExtra("trem1", trem1);
		               myIntent2.putExtra("trem2", trem2);
		               myIntent2.putExtra("trem3", trem3);
		               myIntent2.putExtra("tremolo1", tremolo1);
		               myIntent2.putExtra("tremolo2", tremolo2);
		               myIntent2.putExtra("tremolo3", tremolo3);
		               myIntent2.putExtra("tr_sub1", tr_sub1);
		               myIntent2.putExtra("tr_sub2", tr_sub2);
		               myIntent2.putExtra("tr_sub3", tr_sub3);
		               ampx=0;
		               this.finish();
		               startActivityForResult(myIntent2, 0);
			    	
		               this.finish();
			    	return true;
			 }
		  return true;
		 }

	/*
		 @Override
		  public void onStart() {
		    super.onStart();
		    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
		  }

		  @Override
		  public void onStop() {
		    super.onStop();
		    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
		    displayInterstitial();
		  }
		  
		  public void displayInterstitial() {
			    if (interstitial.isLoaded()) {
			      interstitial.show();
			    }
			  }
		  */

}